<?php
// Címsor
$_['heading_title']	   = 'Hibanapló';

// Szöveg
$_['text_success']	   = 'A hibanapló sikeresen törölve!';
$_['text_list']        = 'Hibalista';

// Hiba
$_['error_warning']	   = 'A hibanapló fájl túl nagy. Törölje a tartalmat!';
$_['error_permission'] = 'Nincs jogosultsága a művelet végrehajtásához!';