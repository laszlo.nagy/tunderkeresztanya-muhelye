<?php
// Címsor
$_['heading_title']    = 'Mentés / Helyreállítás ';

// Szöveg
$_['text_success']     = 'Az adatbázis importálás sikeres!';

// Entry
$_['entry_progress']   = 'Haladás';
$_['entry_export']     = 'Exportálás';

// Tab
$_['tab_backup']       = 'Mentés';
$_['tab_restore']      = 'Visszaállítás';

// Hiba
$_['error_permission'] = 'Nincs jogosultsága a művelet végrehajtásához!';
$_['error_export']     = 'Legalább egy táblát kell kiválasztania az exportáláshoz!';
$_['error_file']       = 'A fájl nem található!';