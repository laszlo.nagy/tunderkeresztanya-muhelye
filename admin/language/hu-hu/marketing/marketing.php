<?php
// Heading
$_['heading_title']     = 'Marketing';

// Text
$_['text_success']      = 'A módosítás sikeresen megtörtént!';
$_['text_list']         = 'Áttekintés';
$_['text_add']          = 'Hozzáadás';
$_['text_edit']         = 'Módosítás';

// Column
$_['column_name']       = 'Kampány neve';
$_['column_code']       = 'Kód';
$_['column_clicks']     = 'Klikkelések';
$_['column_orders']     = 'Rendelések';
$_['column_date_added'] = 'Hozzáadva';
$_['column_action']     = 'Művelet';

// Entry
$_['entry_name']        = 'Kampány neve';
$_['entry_description'] = 'Kampány leírása';
$_['entry_code']        = 'Követőkód';
$_['entry_example']     = 'Példák';
$_['entry_date_added']  = 'Hozzáadva';

// Help
$_['help_code']         = 'Ezzel a kóddal nyomon követheted a marketing kampányod.';
$_['help_example']      = 'A kampány követéséhez az alábbi URL címhez kell hozzárendelned a követő kódot';

// Error
$_['error_permission']  = 'Nincs jogosultságod ehhez a művelethez!';
$_['error_name']        = 'Ez a bejegyzés legalább 1 és legfeljebb 32 karakterből állhat!';
$_['error_code']        = 'A követőkód megadása kötelező!';
$_['error_exists']      = 'A követő kód már használatban van egy másik kampányban!';