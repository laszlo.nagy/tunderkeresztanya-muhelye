<?php
// Heading
$_['heading_title']          = 'Tulajdonságok';

// Text
$_['text_success']           = 'A módosítás sikeresen megtörtént!';
$_['text_list']              = 'Áttekintés';
$_['text_add']               = 'Hozzáadás';
$_['text_edit']              = 'Módosítás';

// Column
$_['column_name']            = 'Megnevezés';
$_['column_attribute_group'] = 'Csoport';
$_['column_sort_order']      = 'Sorrend';
$_['column_action']          = 'Művelet';

// Entry
$_['entry_name']            = 'Megnevezés';
$_['entry_attribute_group'] = 'Csoport';
$_['entry_sort_order']      = 'Sorrend';

// Error
$_['error_permission']      = 'Nincs jogosultságod ehhez a művelethez!';
$_['error_attribute_group']  = 'A tulajdonság csoport megadása kötelező!';
$_['error_name']            = 'Ez a bejegyzés legalább 3 és legfeljebb 64 karakterből állhat!';
$_['error_product']         = 'Ezt a tételt nem törölheted, mert hozzárendelték %s termékhez!';