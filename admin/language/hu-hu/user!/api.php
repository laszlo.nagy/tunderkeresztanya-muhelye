<?php
// Címsor
$_['heading_title']        = 'API';

// Szöveg
$_['text_success']         = 'A módosítás sikeresen megtörtént!';
$_['text_list']            = 'Áttekintés';
$_['text_add']             = 'Hozzáadás';
$_['text_edit']            = 'Módosítás';
$_['text_ip']              = 'Below you can create a list of IP\'s allowed to access the API. Your current IP is %s';

// Oszlop
$_['column_username']      = 'API felhasználónév';
$_['column_status']        = 'Státusz';


$_['column_token']         = 'Token';
$_['column_ip']            = 'IP';
$_['column_date_added']    = 'Hozzáadva';
$_['column_date_modified'] = 'Módosítva';

$_['column_action']        = 'Művelet';

// Entry
$_['entry_username']       = 'API felhasználónév';
$_['entry_key']            = 'API kulcs';
$_['entry_status']         = 'Státusz';
$_['entry_ip']             = 'IP';

// Hiba
$_['error_permission']     = 'Nincs jogosultsága ehhez a művelethez!';
$_['error_username']       = 'Ez a bejegyzés legalább 3, és legfeljebb 20 karakterből álljon!';
$_['error_key']            = 'Az API kulcs legalább 64, és legfeljebb 256 karakterből álljon!';
$_['error_ip']             = 'Legalább egy IP címet kell hozzáadnia az engedéylezett listához!';