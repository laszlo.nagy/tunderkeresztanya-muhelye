<?php
define('DIR_PROD_EDIT_IMAGES', 'C:\xampp\htdocs\kbbsz\image\product_editor\\');
define('DIR_PROD_EDIT', 'C:\xampp\htdocs\kbbsz\\');

class ControllerProductEditorImageUpload extends Controller {
	public $servername = "localhost";
	public $username = "root";
	public $password = "";
	public $dbname = "kreativ_babaszoba_2020";

	private $error = array();

	public function index() {

				// Create connection
				$conn = new mysqli($this -> servername,$this -> username, $this -> password, $this -> dbname);
				// Check connection
				if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
				}
				mysqli_set_charset($conn,"utf8");

		
		if(isset($_POST['product'])){
			
		$sql = "insert into kreativ_babaszoba_2020.kbb_1pd_images (tag_name,real_name,parts,path,product_id) VALUES ('".$_POST['image_tag_name']."','".$_POST['image_real_name']."', '".$_POST['image_parts']."', '"."image\\\\\\product_editor\\\\\\main_images\\\\\\". $_FILES['main_image_upload']['name']."','".$_POST['product']."');";
		$result = $conn->query($sql);

			if ($result){
				$target_dir = DIR_PROD_EDIT_IMAGES . "main_images\\";
				$target_file = $target_dir . basename($_FILES["main_image_upload"]["name"]);
				$uploadOk = 1;

				$check = getimagesize($_FILES["main_image_upload"]["tmp_name"]);
				if($check !== false) {
				 //	echo "File is an image - " . $check["mime"] . ".";
					$uploadOk = 1;
				} else {
				//		echo "File is not an image.";
					$uploadOk = 0;
				}

				if (file_exists($target_file)) {
				//	echo "Sorry, file already exists.";
					$uploadOk = 0;
				  }

				  // Check if $uploadOk is set to 0 by an error
					if ($uploadOk == 0) {
					//	echo "Sorry, your file was not uploaded.";
					// if everything is ok, try to upload file
					} else {
						if (move_uploaded_file($_FILES["main_image_upload"]["tmp_name"], $target_file)) {
						// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
						} else {
					//	echo "Sorry, there was an error uploading your file.";
						}
					}

					$sql = "SELECT a.product_id,a.name,b.id,b.tag_name,b.real_name,b.parts,b.path,b.active from kreativ_babaszoba_2020.kbb_product_description a
					left join kreativ_babaszoba_2020.kbb_1pd_images b on a.product_id = b.product_id
					  where b.Active = 1;";
					$result = $conn->query($sql);
			
					if ($result->num_rows > 0) {
						while($row = $result->fetch_assoc()) {
						  $main_products[] = $row;
						}
					  }
		  
				  
					$data['main_products'] = $main_products;

					$this->document->setTitle('Termékfeltöltés');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('ProductEditor/image_upload', $data));
		
			}
		}
		else if (isset($_POST['image_id'])){
	
			if($_POST['path'] == ""){
				$sql = "update kreativ_babaszoba_2020.kbb_1pd_images set tag_name = '".$_POST['tag_name']."', real_name = '".$_POST['product_name']."', parts = '".$_POST['parts']."', active = '".$_POST['active']."' where id = '".$_POST['image_id']."';";
				$result = $conn->query($sql);
			}
			else{
				print_r($_POST);

				$deleteImgPath = "";
				$sql = "select path from kreativ_babaszoba_2020.kbb_1pd_images where id = '".$_POST['image_id']."'";
				$result = $conn->query($sql);

				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
					  $deleteImgPath = $row['path'];
					}
				  }

				  $deleteImgPath = DIR_PROD_EDIT . $deleteImgPath;

				  echo $deleteImgPath;
				  
	
	/* 			  	if (file_exists($deleteImgPath)){
						unlink($deleteImgPath);
					  } */
		
					  // innen folyt a törlés már működik, fel kell tölteni az uj fajlt és updatelni kell a db-t
 



			 	$sql = "update kreativ_babaszoba_2020.kbb_1pd_images set tag_name = '".$_POST['tag_name']."', real_name = '".$_POST['product_name']."', parts = '".$_POST['parts']."', path='image\\\\\\product_editor\\\\\\main_images\\\\\\".$_POST['path']."' active = '".$_POST['active']."' where id = '".$_POST['image_id']."';";
				$result = $conn->query($sql);

				if ($result){


					$target_dir = DIR_PROD_EDIT_IMAGES . "main_images\\";
					$target_file = $target_dir . basename($_FILES["edit_main_image_upload"]["name"]);
					$uploadOk = 1;
	
					$check = getimagesize($_FILES["edit_main_image_upload"]["tmp_name"]);
					if($check !== false) {
					 //	echo "File is an image - " . $check["mime"] . ".";
						$uploadOk = 1;
					} else {
					//		echo "File is not an image.";
						$uploadOk = 0;
					}
	
					if (file_exists($target_file)) {
					//	echo "Sorry, file already exists.";
						$uploadOk = 0;
					  }
	
					  // Check if $uploadOk is set to 0 by an error
						if ($uploadOk == 0) {
						//	echo "Sorry, your file was not uploaded.";
						// if everything is ok, try to upload file
						} else {
							if (move_uploaded_file($_FILES["edit_main_image_upload"]["tmp_name"], $target_file)) {
							// echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
							} else {
						//	echo "Sorry, there was an error uploading your file.";
							}
						}
	
						$sql = "SELECT a.product_id,a.name,b.id,b.tag_name,b.real_name,b.parts,b.path,b.active from kreativ_babaszoba_2020.kbb_product_description a
						left join kreativ_babaszoba_2020.kbb_1pd_images b on a.product_id = b.product_id
						  where b.Active = 1;";
						$result = $conn->query($sql);
				
						if ($result->num_rows > 0) {
							while($row = $result->fetch_assoc()) {
							  $main_products[] = $row;
							}
						  }
			  
					  
						$data['main_products'] = $main_products;
	
			
				}
				
			}
 



		}
		else if (!isset($_POST['flag']) && !isset($_POST['product']))  {

		  $sql = "SELECT a.product_id,a.name,b.id,b.tag_name,b.real_name,b.parts,b.path,b.Active from kreativ_babaszoba_2020.kbb_product_description a
		  left join kreativ_babaszoba_2020.kbb_1pd_images b on a.product_id = b.product_id
			where b.Active = 1;";
		  $result = $conn->query($sql);
  
		  if ($result->num_rows > 0) {
			  while($row = $result->fetch_assoc()) {

				$main_products[] = $row;
			  }
			}

	
		  $data['main_products'] = $main_products;

		


		$this->document->setTitle('Termékfeltöltés');
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('ProductEditor/image_upload', $data));

		}


	}
	







}