 // image uploader
  const queryString = window.location.search;
 const image_upload_url = 'index.php?route=ProductEditor/image_upload&user_token=';
 const urlParams = new URLSearchParams(queryString);

 var user_token = urlParams.get('user_token')

  // loading screen
  $(document).ajaxStart(function() {
    $(document.body).css({'cursor' : 'wait'});
}).ajaxStop(function() {
    $(document.body).css({'cursor' : 'default'});
});

$('select').on('change', function(e){
   $("#image_real_name").val($( "select option:selected" ).text());
  });


  function fillEditFields(obj){
      var result = [];
    result['image_id'] = obj.parentElement.parentElement.firstChild.nextElementSibling.innerHTML;
    result ['product_name'] = obj.parentElement.parentElement.firstChild.nextElementSibling.nextElementSibling.innerHTML;
    result ['tag_name'] = obj.parentElement.parentElement.firstChild.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML;
    result ['parts'] = obj.parentElement.parentElement.firstChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML;
    result ['path'] = obj.parentElement.parentElement.firstChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.firstChild.src;
    result ['active'] = obj.parentElement.parentElement.firstChild.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.nextElementSibling.innerHTML;

    return result; 

  }


  $('#image_editor_modal').on('shown.bs.modal', function (e) {

    var id = $(e.relatedTarget).data('id');
    var edit_button = $("#"+id)[0];
    var product_data = fillEditFields(edit_button);

    $("#edit_image_id").val(product_data['image_id']);
    $("#edit_image_real_name").val(product_data['product_name']);
    $("#edit_image_tag_name").val(product_data['tag_name']);
    $("#edit_image_parts").val(product_data['parts']);
    $("#edit_image_preview").attr("src",product_data['path'])
    if (product_data['active'] == "Aktív"){
        $("#edit_active_check").prop('checked', true);
    }
    else{
        $("#edit_active_check").prop('checked', false);
    }
  
    

  });

  $("#modalSave").click(function(){
    savedata = {};
    savedata['image_id'] =  $("#edit_image_id").val();
    savedata['product_name'] =  $("#edit_image_real_name").val();
    savedata['tag_name'] =  $("#edit_image_tag_name").val();
    savedata['parts'] =  $("#edit_image_parts").val();
    savedata['path'] =  $("#edit_main_image_upload").val();
    if ($('#edit_active_check').is(":checked"))
        {
            savedata['active'] = 1;
        }
        else{
            savedata['active'] = 0;
        }


        $.ajax({
            url: image_upload_url+urlParams,
            data: savedata,
            type: 'post',
            success: function(data) {
              alert(data);
            }
          });

  });

  function readURL(input,target) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $(target).attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }
  
  $("#edit_main_image_upload").change(function() {
    readURL(this,'#edit_image_preview');
  });

  $("#main_image_upload").change(function() {
    readURL(this,'#image_preview');
  });


