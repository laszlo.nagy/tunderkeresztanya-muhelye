<?php

class ControllerInformationProductEditor extends Controller {

	private $error = array();

 	public $servername = "localhost";
	public $username = "root";
	public $password = "";
	public $dbname = "kreativ_babaszoba_2020"; 

	public function index() {
		$this->load->language('information/productEditor');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('information/productEditor')
		);


		$data['store'] = $this->config->get('config_name');
		$data['address'] = nl2br($this->config->get('config_address'));
		$data['geocode'] = $this->config->get('config_geocode');
		$data['geocode_hl'] = $this->config->get('config_language');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['fax'] = $this->config->get('config_fax');
		$data['open'] = nl2br($this->config->get('config_open'));
		$data['comment'] = $this->config->get('config_comment');

		$data['locations'] = array();

		$this->load->model('localisation/location');

/*         $data['description'] = $this->language->get('text_description');    
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom'); */
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		// **************************                  OWN SCRIPT               *********************************



		// Create connection
		$conn = new mysqli($this -> servername,$this -> username, $this -> password, $this -> dbname);
		// Check connection
		if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
		}
		mysqli_set_charset($conn,"utf8");

		$sql = "SELECT a.product_id,round(a.price,0) as price, round(a.weight,0) as weight,(select unit from kreativ_babaszoba_2020.kbb_weight_class_description where weight_class_id = a.weight_class_id) as weight_unit,round(a.length,0) as length, round(a.width,0) as width,round(a.height,0) as height,
		(select unit from kreativ_babaszoba_2020.kbb_length_class_description where length_class_id = a.length_class_id) as length_unit,b.name,b.description,c.id,c.tag_name,c.real_name,c.parts,c.path
		 FROM kreativ_babaszoba_2020.kbb_product a 
		left join kreativ_babaszoba_2020.kbb_product_description b on a.product_id = b.product_id 
		left join kreativ_babaszoba_2020.kbb_1pd_images c on a.product_id = c.product_id
		where c.active = 1;";
		$result = $conn->query($sql);

		if ($result->num_rows > 0) {
			while($row = $result->fetch_assoc()) {
				$row['description'] = html_entity_decode($row['description']);
			  $main_products[] = $row;
			}
		  }

		  $sql = "SELECT a.product_id,c.name,b.text
		   FROM kreativ_babaszoba_2020.kbb_product a 
		   left join kreativ_babaszoba_2020.kbb_product_attribute b on a.product_id = b.product_id
		   left join kreativ_babaszoba_2020.kbb_attribute_description c on b.attribute_id = c.attribute_id
		   left join kreativ_babaszoba_2020.kbb_1pd_images d on a.product_id = d.product_id
		   where d.active = 1;";
		   $result = $conn->query($sql);
  
		  if ($result->num_rows > 0) {
			  while($row = $result->fetch_assoc()) {
				$main_attributes[] = $row;
			  }
			}

		
		  $data['main_products'] = $main_products;
		  $data['main_attributes'] = $main_attributes;



		
		if (!isset($_POST['product_id']) && !isset($_POST['product_sample_id']) && !isset($_POST['image_part_id'])){
			$this->response->setOutput($this->load->view('information/productEditor', $data));
		}else{ 
			if (isset($_POST['product_sample_id'])){
			
				$html = '';
				
					// Selecting the correct patterns for the choosen part of the product
				  $sql = "SELECT * from kreativ_babaszoba_2020.kbb_1pd_image_patterns where image_part_sample_id = '". $_POST['product_sample_id']."'";
				  $result = $conn->query($sql);
			  
				  if ($result->num_rows > 0) {
					  while($row = $result->fetch_assoc()) {
						$product_patterns[] = $row;
					  }
					}

					$html .= '<div style="border-top: 2px solid #ebebeb " id="patternCarousel">';


				foreach($product_patterns as $prod_pattern){

					if ($prod_pattern['sort_order'] == 1){

						$html .= '    <img onclick="fillWithThisPattern(this.src,this.id)" id="'.$prod_pattern['image_part_sample_id'].'" data-toggle="tooltip" data-placement="top" title="'.$prod_pattern['name'].'" class="float-left sample_images" width="75px" height="75px" src="'.$prod_pattern['path'].'" alt="'.$prod_pattern['name'].'">';
					}else{
						$html .= '    <img onclick="fillWithThisPattern(this.src,this.id)" id="'.$prod_pattern['image_part_sample_id'].'" data-toggle="tooltip" data-placement="top" title="'.$prod_pattern['name'].'" class="float-left sample_images" width="75px" height="75px" src="'.$prod_pattern['path'].'" alt="'.$prod_pattern['name'].'">';
					}
					

			}	


				$this->response->setOutput($html);

			}else if (isset($_POST['product_id'])){

				// Create connection
				$conn = new mysqli($this -> servername,$this -> username,$this -> password, $this -> dbname);
				// Check connection
				if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
				}
				mysqli_set_charset($conn,"utf8");
			
				// Product details image parts etc.
				$sql = "SELECT * from kreativ_babaszoba_2020.kbb_1pd_image_parts where image_id = (select id from kreativ_babaszoba_2020.kbb_1pd_images where tag_name = '". $_POST['product_id']."');";
				$result = $conn->query($sql);
			
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
					  $product_details[] = $row;
					}
				  }


				  // Product master datas 
				  $sql = "SELECT * from kreativ_babaszoba_2020.kbb_1pd_images where tag_name = '". $_POST['product_id']."'";
				  $result = $conn->query($sql);
			  
				  if ($result->num_rows > 0) {
					  while($row = $result->fetch_assoc()) {
						$product[] = $row;
					  }
					}



				// Product images sample to choose parts
				  $sql = "SELECT * from kreativ_babaszoba_2020.kbb_1pd_image_parts_sample where tag_name = '". $_POST['product_id']."'";
				  $result = $conn->query($sql);
			  
				  if ($result->num_rows > 0) {
					  while($row = $result->fetch_assoc()) {
						$product_sample[] = $row;
					  }
					}

					$productData['product'] = $product;
					$productData['product_details'] = $product_details;
					$productData['product_sample'] = $product_sample;
			
					$this->response->setOutput(json_encode($productData,JSON_UNESCAPED_UNICODE));

			} 
			else if (isset($_POST['image_part_id'])){
				// Product details image parts etc.
				$product_details = [];
				$sql = "SELECT * from kreativ_babaszoba_2020.kbb_1pd_image_parts where image_id = (select image_id from kreativ_babaszoba_2020.kbb_1pd_image_parts_sample where id = '". $_POST['image_part_id']."');";
				$result = $conn->query($sql);
			
				if ($result->num_rows > 0) {
					while($row = $result->fetch_assoc()) {
					  $product_details[] = $row;
					}
				  }

				  $productData['product_details'] = $product_details;
				$this->response->setOutput(json_encode($productData,JSON_UNESCAPED_UNICODE));
			}      /// IF POSTING FOR PRODUCT DETAILS MAKE MODAL DATA AND HTML /////////


			
		}
	

	}

	
}

