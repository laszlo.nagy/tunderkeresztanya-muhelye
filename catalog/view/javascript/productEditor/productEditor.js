const productEditorURL = 'http://localhost/kbbsz/index.php?route=information/productEditor';


function patternCaller(element){
    var img = element.getElementsByTagName('img')[0];
  var id = img.getAttribute('id');
  var url = productEditorURL;
  
     $.post(url,
    {
      product_sample_id : id
    },
    function(data, status){ 
  
  
      $('#patterns').html(data);
  
    }); 
  
  }
  
  function fillWithThisPattern(patternUrl, imagePartId){

    console.log('start');
  
    $('#canvasplace').empty();

      var image_part_id = imagePartId;
      var url = productEditorURL;


      
        $.post(url,
        {
          image_part_id: image_part_id
        },
        function(data, status){

  
          
          data = JSON.parse(data);   
          
          for(var i = 0; i < data['product_details'].length; i++) {

            var canvas = document.createElement('canvas');
            canvas.id = 'canvas' + data['product_details'][i]['id'];
            canvas.width = 480;
            canvas.height = 500;
            
            var ctx=canvas.getContext("2d");
            canvas.style.position = "absolute";

            var baseimg=new Image();
            baseimg.src = data['product_details'][i]['path'];

            if (imagePartId == data['product_details'][i]['image_part_sample_id']){
            
                
                var pattern=new Image();
                pattern.src = patternUrl;

                 
                ctx.drawImage(baseimg,0,0);
                ctx.globalCompositeOperation="source-atop";
                ctx.globalAlpha=0.70;

                var madeFromPattern = ctx.createPattern(pattern, 'repeat');

              
      
              ctx.rect(0, 0, canvas.width, canvas.height);
              ctx.fillStyle = madeFromPattern;
              ctx.fill();
              ctx.globalAlpha=.30 ;  
  
              ctx.drawImage(baseimg,0,0);

              localStorage[data['product_details'][i]['image_part_sample_id']] = patternUrl;

            }else{

            var patternUrlFromStorage = localStorage[data['product_details'][i]['image_part_sample_id']];

            if (patternUrlFromStorage != 'default'){


                var pattern=new Image();
                pattern.src = patternUrlFromStorage;

                 
                ctx.drawImage(baseimg,0,0);
                ctx.globalCompositeOperation="source-atop";
                ctx.globalAlpha=0.70;

                var madeFromPattern = ctx.createPattern(pattern, 'repeat');

              
      
              ctx.rect(0, 0, canvas.width, canvas.height);
              ctx.fillStyle = madeFromPattern;
              ctx.fill();
              ctx.globalAlpha=.30 ;  
  
              ctx.drawImage(baseimg,0,0);

            }
            else{
  

              ctx.drawImage(baseimg,0,0);
              ctx.globalCompositeOperation="source-atop";
                ctx.rect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(baseimg,0,0);
                    
            }

            }    

          $('#canvasplace').append(canvas);
    
        
          }
      
        });
   
  
    
  
  } 
  
 
  
  // handle single image modal
  $(".modal-image").on("click", function() {
     $('#imagepreview').attr('src', $(this).find('img').attr('src')); // here asign the image to the modal when the user click the enlarge link
     $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function
  });
  
  // loading screen
  $(document).ajaxStart(function() {
      $(document.body).css({'cursor' : 'wait'});
  }).ajaxStop(function() {
      $(document.body).css({'cursor' : 'default'});
  });
  
  $(".editorLauncher").click(function(){

  var prod_id = $(this).attr("id");
  var url = productEditorURL;
  
    $.post(url,
    {
      product_id: prod_id
    },
    function(data, status){


      data = JSON.parse(data);   
   


      for(i=0;i<data['product_sample'].length;i++){

        localStorage [data['product_sample'][i]['id']] = 'default';
     }  

     drawModalEditor(data);
  $('#productEditorModal').modal({
   show: true
  })

  $( ".sample_editor > div:first-of-type " ).trigger( "click");

 // fillWithThisPattern(, 1);
    });
  });
  
  function drawModalEditor(data){
    baseImagesArray = [];
  
    for(var i = 0; i < data['product_details'].length; i++) {
      
      var baseimg = new Image(); 

        baseimg.src = data['product_details'][i]['path'];
   
      baseImagesArray.push(baseimg);

    }
  
    $('#productEditorModal').empty();

    

  
  // header 
   html = '<div class="modal-dialog modal-dialog-centered" role="document">' + 
                      ' <div class="modal-content">' + 
                       ' <div class="modal-header">' + 
                                 '	<button type="button" class="close" data-dismiss="modal" aria-label="Close">' + 
                          '  <span aria-hidden="true">&times;</span>' + 
                      '	</button>' + 
                      '	<h5 class="modal-title" id="exampleModalLongTitle">' + data['product'][0]['real_name'] + '</h5>' + 
              ' </div>' + 
                       ' <div class="modal-body" style="display: inline-block; position:relative; padding:0px;">';
  
   // main image
    html  += '<div id="modal-body-inner" class="col-xs-10 col-md-10"><div id="canvasplace" style="width: 500px; height: 500px;"></div></div>';
    html  += '<div class="col-xs-2 col-md-2 sample_editor"></div>';
    html += '</div>'+
    '<div id="patterns"></div>'+
    '<div class="modal-footer">'+
    '<div id="product"> '+
		'<div class="form-group">'+
		'<label class="control-label" for="input-quantity">Darab</label>'+
		'<div class="quantity-box">'+
		'<input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control">'+
		'<input type="button" id="plus" value="+" class="form-control">'+
		'<input type="button" id="minus" value="-" class="form-control">	'+				
		'</div>'+
		'<input type="hidden" name="product_id" value="'+ data['product'][0]['product_id'] +'">  '+            
		'<button type="button" id="button-cart" data-loading-text="Betöltés..." class="btn"><i class="ion-bag"></i>Kosárba tesz</button>'+
		'<div class="btn-actions">'+
		'<button class="btn-wishlist" type="button" data-toggle="tooltip" title="" onclick="wishlist.add(\''+ data['product'][0]['real_name'] +'\');" data-original-title="Kívánságlistára"><i class="ion-android-favorite-outline"></i></button>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div>'+
    '</div> '; 
    html += '<div id="patternCarousel"></div>';

    $('#productEditorModal').append(html);
  
                       
             // This loop putting canvas on top of each other and making the main image
  
             for(var i = 0; i < data['product_details'].length; i++) {
                        
              
              var canvas = document.createElement('canvas');
              canvas.id = 'canvas' + data['product_details'][i]['id'];
              canvas.width = 480;
              canvas.height = 500;
              
              canvas.style.position = "absolute";
  
              var ctx=canvas.getContext("2d");
              
           
                ctx.drawImage(baseImagesArray[i],0,0);
         
            
                
                  ctx.globalCompositeOperation="source-atop";
          
                  ctx.rect(0, 0, canvas.width, canvas.height);
      

                  ctx.drawImage(baseImagesArray[i],0,0);
  
            
    
                $('#canvasplace').append(canvas);
               
              }
  
  
              // This loop generating the images on the model right side 
  
              for(var i = 0; i < data['product_sample'].length; i++) {
                html = '<div onclick="patternCaller(this)" class="sample_images"><img class="sample_images" id="' + data['product_sample'][i]['id'] + '" src="' + data['product_sample'][i]['path'] + '"></div><div class="separator"></div>';
              
                $('.sample_editor').append(html);
  
              }


              var minimum = 1;
              $("#input-quantity").change(function(){
                if ($(this).val() < minimum) {

                  $("#input-quantity").val(minimum);
                }
              });
                // increase number of product
              function minus(minimum){
                var currentval = parseInt($("#input-quantity").val());
                $("#input-quantity").val(currentval-1);
                if($("#input-quantity").val() <= 0 || $("#input-quantity").val() < minimum){
    
                  $("#input-quantity").val(minimum);
                }
                };
                // decrease of product
              function plus(){
                var currentval = parseInt($("#input-quantity").val());
                $("#input-quantity").val(currentval+1);
              };
              $('#minus').click(function(){
                minus(minimum);
              });
              $('#plus').click(function(){
                plus();
              });


              // add to cart function
              $('#button-cart').on('click', function() {
                $.ajax({
                  url: 'index.php?route=checkout/cart/add',
                  type: 'post',
                  data: $('#product input[type=\'text\'],#product input[type=\'hidden\'],#product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
                  dataType: 'json',
                  beforeSend: function() {
                    $('#button-cart').button('loading');
                  },
                  complete: function() {
                    $('#button-cart').button('reset');
                  },
                  success: function(json) {
                    $('.alert-dismissible, .text-danger').remove();
                    $('.form-group').removeClass('has-error');
              
                    if (json['error']) {
                      if (json['error']['option']) {
                        for (i in json['error']['option']) {
                          var element = $('#input-option' + i.replace('_', '-'));
              
                          if (element.parent().hasClass('input-group')) {
                            element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                          } else {
                            element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                          }
                        }
                      }
              
                      if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                      }
              
                      // Highlight any found errors
                      $('.text-danger').parent().addClass('has-error');
                    }
              
                    if (json['success']) {
                      $('body').before('<div class="alert alert-success alert-dismissible">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
              
                      $('#cart > button').html('<i class="ion-bag"></i><span id="cart-total">' + json['total'] + '</span>');
              
                      $('html, body').animate({ scrollTop: 0 }, 'slow');
              
                      $('#cart > ul').load('index.php?route=common/cart/info ul li');
                    }
                  },
                      error: function(xhr, ajaxOptions, thrownError) {
                          alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                      }
                });
              });

              // tooltips
              $(function () {
                $('[data-toggle="tooltip"]').tooltip()
              })
  }


  