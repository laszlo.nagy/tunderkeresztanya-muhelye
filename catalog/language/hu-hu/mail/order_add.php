<?php
// Szöveg
$_['text_subject']          = '%s - Megrendelés %s';
$_['text_greeting']         = 'Köszönjük, hogy nálunk vásárolt! A rendelését megkaptuk és a feldolgozását azonnal elkezdjük.';
$_['text_link']             = 'A rendelése megtekintéséhez kattintson az alábbi linkre:';
$_['text_order_detail']     = 'Megrendelés részletei';
$_['text_instruction']      = 'Magyarázat';
$_['text_order_id']         = 'Megrendelés azonosító:';
$_['text_date_added']       = 'Megrendelés dátuma:';
$_['text_order_status']     = 'Megrendelés állapota:';
$_['text_payment_method']   = 'Fizetési mód:';
$_['text_shipping_method']  = 'Szállítási mód:';
$_['text_email']            = 'E-mail:';
$_['text_telephone']        = 'Telefonszám:';
$_['text_ip']               = 'IP cím:';
$_['text_payment_address']  = 'Fizetési cím';
$_['text_shipping_address'] = 'Szállítási cím';
$_['text_products']         = 'Termékek';
$_['text_product']          = 'Termék';
$_['text_model']            = 'Cikkszám';
$_['text_quantity']         = 'Mennyiség';
$_['text_price']            = 'Fogy. ára';
$_['text_order_total']      = 'Rendelés összesen';
$_['text_total']            = 'Összesen';
$_['text_download']         = 'Miután a megrendelését megerősítette, az alábbi linkre kattintva elérheti a letölthető terméke(ke)t:';
$_['text_comment']          = 'A megrendelésre vonatkozó megjegyzések:';
$_['text_footer']           = 'Ha bármilyen kérdése van, válaszoljon erre az e-mailre.';