<?php
// Szöveg
$_['text_subject']  = 'Új jelszava a(z) %s áruházban';
$_['text_greeting'] = 'Új jelszó lett igényelve a(z) %s felhasználói fiókhoz.';
$_['text_change']   = 'A jelszó visszaállításához kattintson az alábbi linkre:';
$_['text_ip']       = 'Az IP cím amihez a kérés használták:';