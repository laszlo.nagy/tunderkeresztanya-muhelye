<?php
// Szöveg
$_['text_subject']      = '%s - Megrendelés frissítése %s';
$_['text_order_id']     = 'Megrendelés azonosító:';
$_['text_date_added']   = 'Megrendelés dátuma:';
$_['text_order_status'] = 'A megrendelése a következő állapotra frissült:';
$_['text_comment']      = 'A megrendelésre vonatkozó megjegyzések:';
$_['text_link']         = 'A megrendelés megtekintéséhez kattintson az alábbi linkre:';
$_['text_footer']       = 'Ha bármilyen kérdése van, válaszoljon erre az e-mailre.';