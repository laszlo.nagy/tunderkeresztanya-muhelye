<?php
// Szöveg
$_['text_subject']        = '%s - Köszönjük a regisztrációt';
$_['text_welcome']        = 'Üdvözöljük és köszönjük, hogy regisztrált a(z) %s web áruházban!';
$_['text_login']          = 'A fiókja elkészült. Az e-mail címével és jelszavával máris be tud lépni, vagy kattintson az alábbi hivatkozásra:';
$_['text_approval']       = 'Fiókját még jóvá kell hagyni, csak ezután tud belépni. A jóváhagyása után e-mail címével és jelszavával tud belépni, illetve az alábbi hivatkozásra kattintással:';
$_['text_service']        = 'Belépés után olyan szolgáltatásokat érhet el, mint a rendelései nyomon követése, számlák újranyomtatása és fiók adatainak szerkesztése.';
$_['text_thanks']         = 'Köszönjük,';
$_['text_new_customer']   = 'Új ügyfél';
$_['text_signup']         = 'Az új ügyfél belépett, mint:';
$_['text_customer_group'] = 'Ügyfél csoport:';
$_['text_firstname']      = 'Keresztnév:';
$_['text_lastname']       = 'Vezetéknév:';
$_['text_email']          = 'E-mail:';
$_['text_telephone']      = 'Telefonszám:';