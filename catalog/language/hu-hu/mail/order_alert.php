<?php
// Szöveg
$_['text_subject']      = '%s - Megrendelés %s';
$_['text_received']     = 'Megrendelés érkezett.';
$_['text_order_id']     = 'Megrendelés azonosító:';
$_['text_date_added']   = 'Megrendelés dátuma:';
$_['text_order_status'] = 'Megrendelés állapota:';
$_['text_product']      = 'Termékek';
$_['text_total']        = 'Összesen';
$_['text_comment']      = 'A megrendelésre vonatkozó megjegyzések:';
