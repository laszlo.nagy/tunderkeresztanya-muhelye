<?php
// Szöveg
$_['text_subject']		        = 'Köszöntjük a(z) %s partner programjában';
$_['text_welcome']		        = 'Köszönjük, hogy csatlakozott a(z) %s partner programjához!';
$_['text_login']                = 'A fiókja elkészült és az e-mail címével és jelszavával máris be tud lépni, vagy kattintson az alábbi hivatkozásra:';
$_['text_approval']		        = 'Fiókjának engedélyezettnek kell lennie a belépéshez. Engedélyezés után beléphet az e-mail címével és jelszavával a web áruházunkban vagy a következő hivatkozáson:';
$_['text_services']		        = 'Belépés után készíthet követő kódokat, nyomon követheti a jutalék kifizetéseket és szerkesztheti fiókjának adatait.';
$_['text_thanks']		        = 'Köszönjük,';
$_['text_new_affiliate']        = 'Új partner';
$_['text_signup']		        = 'Új partner bejelentkezett:';
$_['text_website']		        = 'Weboldal:';
$_['text_customer_group']		= 'Ügyfél csoport:';
$_['text_firstname']	        = 'Keresztnév:';
$_['text_lastname']		        = 'Vezetéknév:';
$_['text_company']		        = 'Cégnév:';
$_['text_email']		        = 'E-mail:';
$_['text_telephone']	        = 'Telefonszám:';