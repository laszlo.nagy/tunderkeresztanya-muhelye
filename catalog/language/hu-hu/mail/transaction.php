<?php
// Szöveg
$_['text_subject']  = '%s - Partner jutalék';
$_['text_received'] = 'Gratulálunk! Ön jutalákot kapott a(z) %s partner programtól.';
$_['text_amount']   = 'Beérkezett:';
$_['text_total']    = 'Az aktuális teljes jutalék összege:';