<?php
// Címsor
$_['heading_title']    = 'Karbantartás';

// Szöveg
$_['text_maintenance'] = 'Karbantartás';
$_['text_message']     = '<h1 style="text-align:center;">Sajnáljuk, de web áruházunk karbantartás miatt nem fogad látogatókat. Kérjük, látogasson vissza később.</h1>';