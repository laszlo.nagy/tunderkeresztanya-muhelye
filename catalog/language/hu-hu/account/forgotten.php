<?php
// Címsor
$_['heading_title']   = 'Elfelejtette a jelszavát?';

// Szöveg
$_['text_account']    = 'Fiók';
$_['text_forgotten']  = 'Elfelejtett jelszó';
$_['text_your_email'] = 'Az Ön e-mail címe';
$_['text_email']      = 'Adja meg a fiókjához tartozó e-mail címét. Majd kattintson a Tovább gombra és küldjük Önnek e-mailben az új jelszavát.';
$_['text_success']    = 'Az új jelszavát elküldtük az e-mail címére.';

// Bejegyzés
$_['entry_email']     = 'E-mail cím';
$_['entry_password']  = 'Új jelszó';
$_['entry_confirm']   = 'Megerősítés';

// Hiba
$_['error_email']     = 'A megadott e-mail cím nem szerepel adatbázisunkban! Kérjük, hogy próbálja meg újra!';
$_['error_approved']  = 'A belépés csak a fiók jóváhagyása után lehetséges.';
$_['error_password']  = 'A jelszó minimum 4 és maximum 20 karakterből állhat!';
$_['error_confirm']   = 'A jelszó megerősítése nem egyezik meg a jelszóval!';