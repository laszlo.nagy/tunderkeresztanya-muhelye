<?php
// Címsor
$_['heading_title']      = 'Fiókom';

// Szöveg
$_['text_account']       = 'Fiók';
$_['text_my_account']    = 'Fiók beállítások';
$_['text_my_orders']     = 'Rendeléseim';
$_['text_my_affiliate']   = 'Partner program fiókom';
$_['text_my_newsletter'] = 'Hírlevél';
$_['text_edit']          = 'Belépési adatok módosítása';
$_['text_password']      = 'Jelszó csere';
$_['text_address']       = 'Számlázási adatok módosítása';
$_['text_credit_card']   = 'Mentett bankkártya adatok kezelése';
$_['text_wishlist']      = 'Kívánságlista módosítása';
$_['text_order']         = 'Eddigi rendeléseim';
$_['text_download']      = 'Letöltések';
$_['text_reward']        = 'Hűségpontok';
$_['text_return']        = 'Visszárú kéréseim';
$_['text_transaction']   = 'Tranzakcióim';
$_['text_newsletter']    = 'Feliratkozás/leiratkozás a hírlevélről';
$_['text_recurring']     = 'Visszatérő fizetések';
$_['text_transactions']  = 'Tranzakciók';
$_['text_affiliate_add']  = 'Partner program regisztráció';
$_['text_affiliate_edit'] = 'A partner program információm szerkesztése';
$_['text_tracking']       = 'Egyedi partner követő kód';