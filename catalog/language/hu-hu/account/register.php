<?php
// Címsor
$_['heading_title']        = 'Regisztráció';

// Szöveg
$_['text_account']         = 'Fiók';
$_['text_register']        = 'Regisztráció';
$_['text_account_already'] = 'Ha már regisztrált tagunk, akkor kérjük, hogy jelentkezzen be a <a href="%s">bejelentkezési</a> oldalon.';
$_['text_your_details']    = 'Személyes adatok';
$_['text_newsletter']      = 'Hírlevél';
$_['text_your_password']   = 'Jelszó';
$_['text_agree']           = 'Elfogadom a(z) <a href="%s" class="agree"><b>%s</b></a> szabályzatot.';

// Bejegyzés
$_['entry_customer_group'] = 'Ügyfél csoport';
$_['entry_firstname']      = 'Keresztnév';
$_['entry_lastname']       = 'Vezetéknév';
$_['entry_email']          = 'E-mail';
$_['entry_telephone']      = 'Telefonszám';
$_['entry_newsletter']     = 'Feliratkozás';
$_['entry_password']       = 'Jelszó';
$_['entry_confirm']        = 'Jelszó megint';

// Hiba
$_['error_exists']         = 'Ezzel az e-mail címmel már regisztráltak!';
$_['error_firstname']      = 'A keresztnév minimum 1 és maximum 32 karakterből állhat!';
$_['error_lastname']       = 'A vezetéknév minimum 1 és maximum 32 karakterből állhat!';
$_['error_email']          = 'A megadott e-mail cím helytelen!';
$_['error_telephone']      = 'A telefonszám minimum 3 és maximum 32 karakterből állhat!';
$_['error_custom_field']   = '%s kötelező mező!';
$_['error_password']       = 'A jelszó minimum 4 és maximum 20 karakterből állhat!';
$_['error_confirm']        = 'A jelszó megerősítése nem egyezik meg a jelszóval!';
$_['error_agree']          = 'El kell fogadnia a(z) %s szabályzatot!';