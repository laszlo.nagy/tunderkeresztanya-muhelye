<?php
// Címsor
$_['heading_title']                         = 'Előfizetések';

// Szöveg
$_['text_account']                          = 'Fiók';
$_['text_recurring']                        = 'Előfizetés';
$_['text_recurring_detail']                 = 'Előfizetés részletei';
$_['text_order_recurring_id']				= 'Előfizetés azonosító:';
$_['text_date_added']                       = 'Létrehozva: ';
$_['text_status']                           = 'Állapot: ';
$_['text_payment_method']                   = 'Fizetési mód: ';
$_['text_order_id']							= 'Rendezés azonosító:';
$_['text_product']                          = 'Termék: ';
$_['text_quantity']                         = 'Mennyiség: ';
$_['text_description']        				= 'Leírás';
$_['text_reference']          				= 'Referencia';
$_['text_transaction']                     = 'Tranzakciók';
$_['text_status_1']                    		= 'Aktív';
$_['text_status_2']                  		= 'Inaktív';
$_['text_status_3']                 		= 'Törölt';
$_['text_status_4']                 		= 'Felfüggesztett';
$_['text_status_5']                   		= 'Lejárt';
$_['text_status_6']                   		= 'Függőben';
$_['text_transaction_date_added']           = 'Létrehozva';
$_['text_transaction_payment']              = 'Fizetés';
$_['text_transaction_outstanding_payment']  = 'Kiemelt fizetés';
$_['text_transaction_skipped']              = 'Fizetés kihagyva';
$_['text_transaction_failed']               = 'Fizetés sikertelen';
$_['text_transaction_cancelled']            = 'Törölt';
$_['text_transaction_suspended']            = 'Felfüggesztett';
$_['text_transaction_suspended_failed']     = 'Sikertelen fizetés miatt felfüggesztve';
$_['text_transaction_outstanding_failed']   = 'Kiemelt fizetés sikertelen';
$_['text_transaction_expired']              = 'Lejárt';
$_['text_empty']                            = 'Nincs élő előfizetése!';
$_['text_error']                 			= 'A kért előfizetési megrendelés nem található!';
$_['text_cancelled']                        = 'Az előfizetés törölve lett!';

// Oszlop
$_['column_date_added']                     = 'Létrehozva';
$_['column_type']                           = 'Típus';
$_['column_amount']                         = 'Összeg';
$_['column_status']                         = 'Állapot';
$_['column_product']                        = 'Termék';
$_['column_order_recurring_id']                   = 'Előfizetés száma';

// Hiba
$_['error_not_cancelled']                   = 'Hiba: %s';
$_['error_not_found']                       = 'Az előfizetést nem lehet törölni!';

// Gomb
$_['button_return']                         = 'Vissza';