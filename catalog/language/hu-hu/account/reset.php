<?php
// Címsor
$_['heading_title']  = 'Jelszó csere';

// Szöveg
$_['text_account']   = 'Fiók';
$_['text_password']  = 'Kérjük, adja meg az új jelszót, amit használni kíván.';
$_['text_success']   = 'Sikeresen megváltoztatta a jelszavát.';

// Bejegyzés
$_['entry_password'] = 'Jelszó';
$_['entry_confirm']  = 'Jelszó megerősítése';

// Hiba
$_['error_password'] = 'A jelszó minimum 4 és maximum 20 karakterből állhat!';
$_['error_confirm']  = 'A jelszó megerősítése nem egyezik meg a jelszóval!';
$_['error_code']     = 'A jelszó törlési kód hibás, vagy már használva lett!';