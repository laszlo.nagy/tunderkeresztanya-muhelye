<?php
// Heading
$_['heading_title']             = 'A partner programban lévő információja';

// Szöveg
$_['text_account']              = 'Fiók';
$_['text_affiliate']            = 'Partner program';
$_['text_my_affiliate']         = 'Partner program fiókom';
$_['text_payment']              = 'Fizetés információk';
$_['text_cheque']               = 'Csekk';
$_['text_paypal']               = 'PayPal';
$_['text_bank']                 = 'Banki átutalás';
$_['text_success']              = 'Sikeresen módosította a fiók adatokat.';
$_['text_agree']                = 'Elolvastam és elfogadom a(z) <a href="%s" class="agree"><b>%s</b></a>-et.';

// Bejegyzés
$_['entry_company']             = 'Cégnév';
$_['entry_website']             = 'Web oldal';
$_['entry_tax']                 = 'Adószám';
$_['entry_payment']             = 'Fizetési mód';
$_['entry_cheque']              = 'Csekk címzett neve';
$_['entry_paypal']              = 'PayPal e-mail cím';
$_['entry_bank_name']           = 'Bank neve';
$_['entry_bank_branch_number']  = 'IBAN szám';
$_['entry_bank_swift_code']     = 'SWIFT kód';
$_['entry_bank_account_name']   = 'Számlatulajdonos neve';
$_['entry_bank_account_number'] = 'Számlaszám';

// Hiba
$_['error_agree']               = 'El kell fogadnia a(z) %s!';
$_['error_cheque']              = 'Csekk címzett nevét kötelező megadni';
$_['error_paypal']              = 'A megadott PayPal e-mail cím helytelen!';
$_['error_bank_account_name']   = 'A számlatulajdonos nevét kötelező megadni!';
$_['error_bank_account_number'] = 'A számlaszámot kötelező megadni!';
$_['error_custom_field']        = 'A(z) %s mezőt kötelező megadni!';