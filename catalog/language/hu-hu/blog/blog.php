<?php
// Text
$_['button_read_more']         		 = 'Olvass tovább';
$_['text_empty']           			 = 'Nincsenek cikkek';
$_['text_headingtitle']           			 = 'Legfrissebb blogbejegyzések';
$_['text_blog'] = 'Blog';
$_['text_subtitle'] = 'Mirum est notare quam littera gothica, quam nunc putamus parum claram anteposuerit litterarum formas.';
