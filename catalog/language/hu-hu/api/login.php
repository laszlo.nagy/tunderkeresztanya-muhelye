<?php
// Szöveg
$_['text_success'] = 'Az API munkamenet sikeresen elindítva!';

// Hiba
$_['error_key']  = 'Hibás API kulcs!';
$_['error_ip']   = 'Az Ön IP címének (%s) nincs jogosultsága az API eléréséhez!';