<?php
// Címsor
$_['heading_title'] = 'Hűségpontok beváltása (elérhető: %s)';

// Szöveg
$_['text_reward']   = 'Hűségpontok (%s)';
$_['text_order_id'] = 'Rendelés azonosító: #%s';
$_['text_success']  = 'A hűségpontok után járó kedvezményét sikeresen beváltotta!';

// Bejegyzés
$_['entry_reward']  = 'Beváltandó pontok (max %s)';

// Hiba
$_['error_reward']  = 'Kérjük, adja meg a beváltani kívánt pontok összegét!';
$_['error_points']  = 'Önnek nincs %s értékű beváltható pontja!';
$_['error_maximum'] = 'A maximálisan beváltható pontok száma: %s!';