<?php
// Címsor
$_['heading_title']        = 'Várható szállítási költségek és adók';

// Szöveg
$_['text_success']         = 'A becsült szállítási költséget használja!';
$_['text_shipping']        = 'Kérjük, adja meg a szállítás helyét, hogy meg tudjuk becsülni a szállítási költséget.';
$_['text_shipping_method'] = 'Kérjük, válassza ki a kívánt szállítási módot ehhez a megrendeléshez.';

// Bejegyzés
$_['entry_country']        = 'Ország';
$_['entry_zone']           = 'Megye';
$_['entry_postcode']       = 'Irányítószám';

// Hiba
$_['error_postcode']       = 'Az irányítószám minimum 2 és maximum 10 karakterből állhat!';
$_['error_country']        = 'Kérjük, válassza ki az országot!';
$_['error_zone']           = 'Kérjük, válassza ki a megyét!';
$_['error_shipping']       = 'A szállítási mód választása kötelező!';
$_['error_no_shipping']    = 'Jelenleg nincsenek választható szállítási módok. Kérjük, lépjen velünk <a href="%s">kapcsolatba</a> a hiba megoldásért!';