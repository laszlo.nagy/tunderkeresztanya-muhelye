<?php
// Címsor
$_['heading_title'] = 'Ajándékutalvány beváltása';

// Szöveg
$_['text_voucher']	= 'Ajándékutalvány (%s)';
$_['text_success']  = 'Sikeresen beváltotta az ajándékutalványát!';

// Bejegyzés
$_['entry_voucher'] = 'Adja meg ajándékutalványa kódját';

// Hiba
$_['error_voucher'] = 'Az ajándékutalvány érvénytelen vagy az egyenleg felhasználásra került!';
$_['error_empty']   = 'Kérjük, adja meg az ajándékutalványa kódját!';