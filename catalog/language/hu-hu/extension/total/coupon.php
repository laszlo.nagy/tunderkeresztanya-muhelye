<?php
// Címsor
$_['heading_title'] = 'Kupon beváltása';

// Szöveg
$_['text_coupon']	= 'Kupon (%s)';
$_['text_success']  = 'Sikeresen beváltotta a kupon kódját!';

// Bejegyzés
$_['entry_coupon']  = 'Adja meg a kupon kódját';

// Hiba
$_['error_coupon']  = 'A kupon érvénytelen, lejárt vagy elérte használati limitjét!';
$_['error_empty']   = 'Kérjük, adja meg a kupon kódot!';