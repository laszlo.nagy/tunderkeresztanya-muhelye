<?php
// Szöveg
$_['text_captcha']  = 'Ellenőrző kód';

// Bejegyzés
$_['entry_captcha'] = 'Kérjük, adja meg az ellenőrző kódot a lenti dobozból';

// Hiba
$_['error_captcha'] = 'Az ellenőrző kód nem egyezik a képen lévővel!';