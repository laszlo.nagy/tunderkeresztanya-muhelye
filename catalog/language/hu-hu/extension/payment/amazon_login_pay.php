<?php
// Címsor
$_['heading_title']				= 'Belépés és fizetés az Amazon-nal';
$_['heading_address']			= 'Kérjük, válasszon szállítási címet';
$_['heading_payment']			= 'Kérjük, válasszon fizetési módot';
$_['heading_confirm']			= 'Rendelés áttekintése';

// Szöveg
$_['text_back']					= 'Vissza';
$_['text_cart']					= 'Kosár';
$_['text_confirm']				= 'Jóváhagyás';
$_['text_continue']				= 'Tovább';
$_['text_lpa']					= 'Belépés és fizetés az Amazon-nal';
$_['text_enter_coupon']			= 'Kérjük, adja meg a kupon kódját. Ha nem rendelkezik ilyennel, akkor hagyja üresen a mezőt.';
$_['text_coupon']				= 'Kupon';
$_['text_tax_other']			= 'Áfa és egyéb kezelési költségek';
$_['text_success_title']		= 'A megrendelése el lett küldve!';
$_['text_payment_success']		= 'A megrendelését megkaptuk, melynek részleteit az alábbiakban találja';

// Hiba
$_['error_payment_method']		= 'Kérjük, válasszon fizetési módot!';
$_['error_shipping']			= 'Kérjük, válasszon szállítási módot!';
$_['error_shipping_address']	= 'Kérjük, válasszon szállítási címet!';
$_['error_shipping_methods']	= 'Hiba történt a cím lekérésénél az Amazon-tól. Kérjük, lépjen kapcsolatba az áruház üzemeltetőjével.';
$_['error_no_shipping_methods'] = 'Nincsenek szállítási módok a kiválasztott címre. Kérjük, válasszon másik szállítási címet.';
$_['error_process_order']		= 'Hiba történt a megrendelés feldolgozása során. Kérjük, lépjen kapcsolatba az áruház üzemeltetőjéve.';
$_['error_login']				= 'Belépési hiba';
$_['error_login_email']			= 'Belépési hiba: %s fiókhoz tartozó e-mail cím nem egyezik meg az Amazon fiókhoz tartozó e-mail címmel!';
$_['error_minimum']             = 'A minimum rendelési összeg az Amazon Pay-nál: %s!';