<?php
// Szöveg
$_['text_title']			 = 'Hitelkártya / Bankkártya (Worldpay)';
$_['text_credit_card']		 = 'Kártya adatai';
$_['text_card_type']		 = 'Kártya típus: ';
$_['text_card_name']		 = 'Kártya név: ';
$_['text_card_digits']		 = 'Utolsó számok: ';
$_['text_card_expiry']		 = 'Lejárat: ';
$_['text_trial']			 = '%s minden %s %s a(z) %s fizetés után ';
$_['text_recurring']		 = '%s minden %s %s';
$_['text_length']			 = ' a(z) %s fizetés(ek)';
$_['text_confirm_delete']	 = 'Biztos, hogy törli ezt a kártyát?';
$_['text_card_success']		 = 'Card has been successfully removed';
$_['text_card_error']		 = 'There are an error removing your card. Please contact the shop administrator for help.';

// Bejegyzés
$_['entry_card']			 = 'Új vagy meglévő kártya: ';
$_['entry_card_existing']	 = 'Meglévő';
$_['entry_card_new']		 = 'Új';
$_['entry_card_save']		 = 'Remember Card Details';
$_['entry_cc_cvc']			 = 'Kártya ellenőrző kód (CVC)';
$_['entry_cc_choice']		 = 'Válasszon a meglévő kártyák közül';

// Gomb
$_['button_delete_card']	 = 'Kártya törlése';

// Hiba
$_['error_process_order']	 = 'There are an error processing your order. Please contact the shop administrator for help.';