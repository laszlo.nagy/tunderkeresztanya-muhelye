<?php

$_['text_title'] 				= 'Hitelkártya / Bankkártya';
$_['button_confirm'] 			= 'Megerősítés';

$_['text_postcode_check'] 		= 'Irányítószám ellenőrzés: %s';
$_['text_security_code_check'] 	= 'CVV2 ellenőrzés: %s';
$_['text_address_check']		= 'Cím ellenőrzés: %s';
$_['text_not_given'] 			= 'Nem átadott';
$_['text_not_checked'] 			= 'Nem ellenőrzött';
$_['text_match'] 				= 'Egyező';
$_['text_not_match'] 			= 'Nem egyezik';
$_['text_payment_details'] 		= 'Tranzakció részletek';

$_['entry_card_type'] 			= 'Kártya típus';