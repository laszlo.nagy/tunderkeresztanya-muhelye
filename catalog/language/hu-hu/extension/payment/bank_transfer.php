<?php
// Szöveg
$_['text_title']				= 'Banki átutalás';
$_['text_instruction']			= 'Banki átutalás ismertetője';
$_['text_description']			= 'Kérjük, utalja át a vásárlás teljes összegét a következő bankszámlára:';
$_['text_payment']				= 'Az Ön rendelését addig nem szállítjuk ki, amíg nem érkezik meg annak ellenértéke.';