<?php
// Címsor
$_['heading_title']              = 'Wechat QR kód fizetés';

// Szöveg
$_['text_title']                 = 'Wechat fizetés';
$_['text_checkout']              = 'Megrendelés';
$_['text_qrcode']                = 'QR kód';
$_['text_qrcode_description']    = 'Kérjük, olvassa be a QR kódot a WeChat alkalmazásba, hogy végre tudja hajtani a fizetést!';
