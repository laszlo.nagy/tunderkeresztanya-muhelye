<?php
// Text
$_['text_title']            = 'Hitelkártya / Bankkártya';
$_['text_card_details']     = 'Kártya adatok';
$_['text_echeck_details']   = 'eCheck részletek';
$_['text_card']             = 'Kártya';
$_['text_echeck']           = 'eCheck';
$_['text_wait']             = 'Kérjük, várjon!';
$_['text_confirm_delete']   = 'Are you sure you want to delete the card?';
$_['text_no_cards']         = 'No cards saved yet';
$_['text_select_card']      = 'Kérjük, válasszon ki kártyát';

// Bejegyzés
$_['entry_method']          = 'Fizetési mód';
$_['entry_card_new_or_old'] = 'Új vagy a meglévő kártya';
$_['entry_card_new']        = 'Új';
$_['entry_card_old']        = 'Meglévő';
$_['entry_card_type']       = 'Kártya típus';
$_['entry_card_number']     = 'Kártya szám';
$_['entry_card_expiry']     = 'Lejárat';
$_['entry_card_cvv2']       = 'CVV2';
$_['entry_card_save']       = 'Kártya adatok mentése';
$_['entry_card_choice']     = 'Válasszon kártyát';
$_['entry_account_number']  = 'Account Number';
$_['entry_routing_number']  = 'Routing Number';

// Gomb
$_['button_confirm']        = 'Confirm Order';
$_['button_delete']         = 'Delete Selected Card';

// Hiba
$_['error_card_number']     = 'Card Number must be between 1 and 19 characters!';
$_['error_card_type']       = 'Card Type is not a valid selection!';
$_['error_card_cvv2']       = 'CVV2 must be between 1 and 4 characters!';
$_['error_data_missing']    = 'Hiányzó adatok!';
$_['error_not_logged_in']   = 'Nincs bejelentkezve!';
$_['error_no_order']        = 'No matching order!';
$_['error_no_post_data']    = 'Nincs $_POST adat.';
$_['error_select_card']     = 'Kérjük, válassza ki a kártyát!';
$_['error_no_card']         = 'No such card found!';
$_['error_no_echeck']       = 'Az eCheck nem támogatott!';
$_['error_account_number']  = 'Account Number must be between 1 and 19 characters!';
$_['error_routing_number']  = 'Routing Number must be between 1 and 9 characters!';
$_['error_not_enabled']     = 'A modul nincs bekapcsolva';