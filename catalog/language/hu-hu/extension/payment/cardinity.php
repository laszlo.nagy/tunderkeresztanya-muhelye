<?php
// Szöveg
$_['text_title']						= 'Hitelkártya / Bankkártya (Cardinity)';
$_['text_payment_success']				= 'Sikeres fizetés, részletek az alábbiakban:';
$_['text_payment_failed']				= 'Sikertelen fizetés, részletek az alábbiakban:';

// Entry
$_['entry_holder']						= 'Kártyatulajdonos neve';
$_['entry_pan']							= 'Kártya szám';
$_['entry_expires']						= 'Lejárat';
$_['entry_exp_month']					= 'Hónap';
$_['entry_exp_year']					= 'Év';
$_['entry_cvc']							= 'CVC';

// Hiba
$_['error_process_order']				= 'There was an error processing your order. Please contact the shop administrator for help.';
$_['error_invalid_currency']			= 'Kérjük, használjon érvényes pénznemet.';
$_['error_finalizing_payment']			= 'Error finalizing payment.';
$_['error_unknown_order_id']			= 'Could not find cardinity payment with this order_id.';
$_['error_invalid_hash']				= 'Invalid hash.';
$_['error_payment_declined']			= 'Payment was declined by issuing bank.';

// Gomb
$_['button_confirm']					= 'Fizetés most';