<?php
// Szöveg
$_['text_title']				= 'Hitel vagy Bankkártya (Web Payment Software)';
$_['text_credit_card']			= 'Bankkártya adatok';

// Bejegyzés
$_['entry_cc_owner']			= 'Kártya kibocsátó';
$_['entry_cc_number']			= 'Kártya szám';
$_['entry_cc_expire_date']		= 'Kártya lejárati dátuma';
$_['entry_cc_cvv2']				= 'Kártya biztonsági kódja (CVV2)';