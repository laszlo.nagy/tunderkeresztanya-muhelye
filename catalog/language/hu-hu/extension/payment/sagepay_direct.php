<?php
// Szöveg
$_['text_title']				= 'Hitelkártya / Bankkártya (SagePay)';
$_['text_credit_card']			= 'Bankkártya adatai';
$_['text_card_type']			= 'Kártya típus: ';
$_['text_card_name']			= 'Kártyán szereplő név: ';
$_['text_card_digits']			= 'Kártya utolsó számai: ';
$_['text_card_expiry']			= 'Kártya lejárati dátuma: ';
$_['text_trial']				= '%s every %s %s for %s payments then ';
$_['text_recurring']			= '%s every %s %s';
$_['text_length']				= ' for %s payments';
$_['text_fail_card']			= 'There was an issue removing your SagePay card, Please contact the shop administrator for help.';
$_['text_confirm_delete']		= 'Are you sure you want to delete the card?';

// Bejegyzés
$_['entry_card']				= 'New or Existing Card: ';
$_['entry_card_existing']		= 'Meglévő';
$_['entry_card_new']			= 'Új';
$_['entry_card_save']			= 'Remember Card Details';
$_['entry_cc_owner']			= 'Card Owner';
$_['entry_cc_type']				= 'Kártya típus';
$_['entry_cc_number']			= 'Kártya szám';
$_['entry_cc_expire_date']		= 'Card Expiry Date';
$_['entry_cc_cvv2']				= 'Kártya biztonsági kód (CVV2)';
$_['entry_cc_choice']			= 'Válasszon a meglévő kártyák közül';

// Gomb
$_['button_delete_card']		= 'Kiválasztott kártya törlése';