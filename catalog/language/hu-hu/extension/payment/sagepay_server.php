<?php
// Szöveg
$_['text_title']				= 'Hitelkártya / Bankkártya (SagePay)';
$_['text_credit_card']			= 'Bankkártya adatai';
$_['text_description']			= 'Items on %s Order No: %s';
$_['text_card_type']			= 'Kártya típus: ';
$_['text_card_name']			= 'Kártyán szereplő név: ';
$_['text_card_digits']			= 'Kártya utolsó számai: ';
$_['text_card_expiry']			= 'Kártya lejárati dátuma: ';
$_['text_trial']				= '%s every %s %s for %s payments then ';
$_['text_recurring']			= '%s every %s %s';
$_['text_length']				= ' for %s payments';
$_['text_success']				= 'Your payment has been authorised.';
$_['text_decline']				= 'Your payment has been declined.';
$_['text_bank_error']			= 'There was an error processing your request with the bank.';
$_['text_transaction_error']	= 'There was an error processing your transaction.';
$_['text_generic_error']		= 'There was an error processing your request.';
$_['text_hash_failed']			= 'Hash check failed. Do not try your payment again as the payment status is unknown. Please contact the merchant.';
$_['text_link']					= 'Please click <a href="%s">here</a> to continue';
$_['text_confirm_delete']		= 'Are you sure you want to delete the card?';

// Bejegyzés
$_['entry_card']				= 'Új vagy meglévő kártya: ';
$_['entry_card_existing']		= 'Meglévő';
$_['entry_card_new']			= 'Új';
$_['entry_card_save']			= 'Remember card details for future use';
$_['entry_cc_choice']			= 'Válasszon a meglévő kártyák közül';

// Gomb
$_['button_delete_card']		= 'Kiválasztott kártya törlése';