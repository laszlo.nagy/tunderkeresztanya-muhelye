<?php
// Szöveg
$_['text_title']                = 'Square';
$_['text_canceled']             = 'Sikeresen visszavonta ezt a fizetést! Küldjük róla a megerősítő e-mail-t.';
$_['text_confirm_cancel']       = 'Biztosan törölni akarja az előfizetést?';
$_['text_order_history_cancel'] = 'Ön megszüntette az előfizetést. Többször nem kerül levonásra az előfizetési díj.';

// Gomb
$_['button_cancel']             = 'Az előfizetés megszüntetése';

// Hiba
$_['error_not_cancelled']       = 'Hiba: %s';
$_['error_not_found']           = 'Az előfizetői profilt nem lehet megszüntetni';