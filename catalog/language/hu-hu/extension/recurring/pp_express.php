<?php
// Szöveg
$_['text_title']          = 'PayPal Expressz fizetés';
$_['text_canceled']       = 'Sikeresen visszavonta ezt a fizetést!';

// Gomb
$_['button_cancel']       = 'Az előfizetés megszüntetése';

// Hiba
$_['error_not_cancelled'] = 'Hiba: %s';
$_['error_not_found']     = 'Az előfizetői profilt nem lehet megszüntetni!';