<?php
// Heading 
$_['heading_title'] 	 = 'Hírlevél';
$_['heading_title2'] 	 = '10% tagsági kedvezmény';
$_['heading_title3'] 	 = 'Kedvezményes ajánlatok a felirakozóknak';
$_['text_colose'] 	 = 'Bezárás';

$_['newletter_des'] 	 = 'Íratkozzon fel hírleveleinkre és értesüljön elsőként legújabb termékeinkről, egyedi ajánlatainkról.';
$_['newletter_des2'] 	 = 'Íratkozzon fel hírleveleinkre és értesüljön elsőként legújabb termékeinkről, egyedi ajánlatainkról.';

//Fields
$_['entry_email'] 		 = 'Kérjük adja meg e-mail címét...';
$_['entry_name'] 		 = 'Név';

//Buttons
$_['entry_button'] 		 = 'Feliratkozás';
$_['entry_unbutton'] 	 = 'Leiratkozás';

//text
$_['text_subscribe'] 	 = 'Feliratkozás itt';

$_['mail_subject']   	 = 'Hírlevél Feliratkozás';

//Messages
$_['error_invalid'] 	 = 'Hiba : Kérjük, létező e-mail címet adjon meg.';
$_['subscribe']	    	 = 'Siker : Sikeres feliratkozás.';
$_['unsubscribe'] 	     = 'Siker : Sikeres leiratkozás.';
$_['alreadyexist'] 	     = 'Hiba : Ez az e-mail cím már létezik.';
$_['notexist'] 	    	 = 'Hiba : Az e-mail cím nem létezik.';
$_['entry_show_again']   =  "Ne mutassa ezt az ablakot többé.";