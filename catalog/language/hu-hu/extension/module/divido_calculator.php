<?php
// Calculator
$_['text_checkout_title']      = 'Részletfizetés';
$_['text_choose_plan']         = 'Válassza ki a díjcsomagját';
$_['text_choose_deposit']      = 'Válassza ki a befizetés értékét';
$_['text_monthly_payments']    = 'havi fizetés a(z)';
$_['text_months']              = 'hónap';
$_['text_term']                = 'ÁSZF';
$_['text_deposit']             = 'Befizetés';
$_['text_credit_amount']       = 'Hitel költsége';
$_['text_amount_payable']      = 'Összesen fizetendő';
$_['text_total_interest']      = 'Összes APR kamat';
$_['text_monthly_installment'] = 'Havi részlet';
$_['text_redirection']         = 'A megrendelés véglegesítéséhez Önt átirányítjuk a Divido oldalára, amit megerősíti a megrendelését';