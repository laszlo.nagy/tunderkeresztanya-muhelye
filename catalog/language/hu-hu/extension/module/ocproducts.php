<?php
$_['heading_title']    = 'Termékek modul';

// Text
$_['text_empty']    = 'Nem található termék';
$_['text_years']    = 'Évek';
$_['text_months']    = 'Hónapok';
$_['text_weeks']    = 'Hetek';
$_['text_days']    = 'Napok';
$_['text_hrs']    = 'Órák';
$_['text_mins']    = 'Percek';
$_['text_secs']    = 'Másodpercek';

$_['text_year']    = 'Év';
$_['text_month']    = 'Hónap';
$_['text_week']    = 'Hét';
$_['text_day']    = 'Nap';
$_['text_hr']    = 'Óra';
$_['text_min']    = 'Perc';
$_['text_sec']    = 'Másodperc';
