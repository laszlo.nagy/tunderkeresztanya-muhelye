<?php

// Heading
$_['heading_title']        = 'Felhasználói fiók regisztrálása';

// Text
$_['text_account']         = 'Felhasználói fiók';
$_['text_register']        = 'Regisztráció';
$_['text_account_already'] = 'Ha már van felhasználói fiókja, kérjük jelentkezzen be itt <a onclick="ocajaxlogin.appendLoginForm()" href="javascript:void(0);">Bejelentkezés</a>.';
$_['text_your_details']    = 'Személyes adatok';
$_['text_your_address']    = 'Cím';
$_['text_newsletter']      = 'Hírlevél';
$_['text_your_password']   = 'Jelszava';
$_['text_agree']           = 'Megértettem és egyetértek a <a href="%s" class="agree"><b>%s</b></a>';

// Entry
$_['entry_customer_group'] = 'Vásárlói csoport';
$_['entry_firstname']      = 'Keresztnév';
$_['entry_lastname']       = 'Vezetéknév';
$_['entry_email']          = 'E-mail';
$_['entry_telephone']      = 'Telefonszám';
$_['entry_fax']            = 'Fax';
$_['entry_company']        = 'Cég';
$_['entry_address_1']      = 'Cím 1';
$_['entry_address_2']      = 'Cím 2';
$_['entry_postcode']       = 'Irányítószám';
$_['entry_city']           = 'Város';
$_['entry_country']        = 'Ország';
$_['entry_zone']           = 'Régió / Megye';
$_['entry_newsletter']     = 'Feliratkozás';
$_['entry_password']       = 'Jelszó';
$_['entry_confirm']        = 'Jelszó megerősítése';

// Error
$_['error_exists']         = 'Figyelmeztetés: Ez az E-mail cím már foglalt!';
$_['error_firstname']      = 'A keresztnév 1-32 karatkerből állhat!';
$_['error_lastname']       = 'A vezetéknév 1-32 karatkerből állhat!';
$_['error_email']          = 'Az E-mail cím formátuma nem megfelelő!';
$_['error_telephone']      = 'A telefonszám 1-32 karatkerből állhat!';
$_['error_address_1']      = 'A cím 3-128 karatkerből állhat!';
$_['error_city']           = 'A város 3-128 karatkerből állhat!';
$_['error_postcode']       = 'Az irányítószám 2-10 karaterből állhat!';
$_['error_country']        = 'Kérjük válasszon országot!';
$_['error_zone']           = 'Kérjük válasszon régiót/megyét!';
$_['error_custom_field']   = 'Ennek a mezőnek a kitöltése kötelező!';
$_['error_password']       = 'A jelszó 4-20 karakterből állhat!';
$_['error_confirm']        = 'A jelszó nem egyezik meg!';
$_['error_agree']          = 'Figyelmeztetés: El kell fogadnod a %s!';