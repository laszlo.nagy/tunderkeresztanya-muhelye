<?php
// Címsor
$_['heading_title']			= 'Fizetés Amazon-nal';

// Szöveg
$_['text_module']			= 'Modulok';
$_['text_success']			= 'Sikeresen módosította az Amazon fizetési módot!';
$_['text_content_top']		= 'Fejléc';
$_['text_content_bottom']	= 'Lábléc';
$_['text_column_left']		= 'Bal oszlop';
$_['text_column_right']		= 'Jobb oszlop';
$_['text_pwa_button']		= 'Fizetés Amazon-nal';
$_['text_pay_button']		= 'Fizetés';
$_['text_a_button']			= 'A';
$_['text_gold_button']		= 'Gold';
$_['text_darkgray_button']	= 'Sötét szürke';
$_['text_lightgray_button'] = 'Világos szürke';
$_['text_small_button']		= 'Kicsi';
$_['text_medium_button']	= 'Közepes';
$_['text_large_button']		= 'Nagy';
$_['text_x_large_button']	= 'Óriási';

// Bejegyzés
$_['entry_button_type']		= 'Gomb típus';
$_['entry_button_colour']	= 'Gomb szín';
$_['entry_button_size']		= 'Gomb méret';
$_['entry_layout']			= 'Kinézet';
$_['entry_position']		= 'Position';
$_['entry_status']			= 'Státusz';
$_['entry_sort_order']		= 'Rendezés';

// Hiba
$_['error_permission']		= 'Nincs jogosultsága az Amazon modul módosítására!';