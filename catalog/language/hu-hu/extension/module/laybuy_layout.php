<?php
// Címsor
$_['heading_title']            = 'Lay-Buy információk';

// Szöveg
$_['text_reference_info']      = 'Reference Information';
$_['text_laybuy_ref_no']       = 'Lay-Buy Reference ID:';
$_['text_paypal_profile_id']   = 'PayPal profil azonosító:';
$_['text_payment_plan']        = 'Payment Plan';
$_['text_status']              = 'Státusz:';
$_['text_amount']              = 'Amount:';
$_['text_downpayment_percent'] = 'Down Payment Percent:';
$_['text_months']              = 'Months:';
$_['text_downpayment_amount']  = 'Down Payment Amount:';
$_['text_payment_amounts']     = 'Payment Amounts:';
$_['text_first_payment_due']   = 'First Payment Due:';
$_['text_last_payment_due']    = 'Last Payment Due:';
$_['text_downpayment']         = 'Down Payment';
$_['text_month']               = 'Hónap';

// Column
$_['column_instalment']        = 'Instalment';
$_['column_amount']            = 'Amount';
$_['column_date']              = 'Dátum';
$_['column_pp_trans_id']       = 'PayPal tranzakició azonosító';
$_['column_status']            = 'Státusz';