<?php

// Heading
$_['heading_title']  = 'Ajax Belépés';

// Text
$_['text_account']                 = 'Felhasználói fiók';
$_['text_login']                   = 'Bejelentkezés';
$_['text_new_customer']            = 'Új vásárló';
$_['text_register']                = 'Fiók regisztráció';
$_['text_register_account']        = 'By creating an account you will be able to shop faster, be up to date on an order\'s status, and keep track of the orders you have previously made.';
$_['text_register_account']        = 'A felhasználói fiók regisztrálásával gyorsabban vásárolhatsz, első kézből értesülhetsz a rendelésed állapotáról illetve nyomon tudod követni megrendeléseidet.';
$_['text_returning_customer']      = 'Visszatérő vásárló';
$_['text_i_am_returning_customer'] = 'Visszatérő vásárló vagyok';
$_['text_forgotten']               = 'Elfelejtett jelszó';
$_['button_register_link']         = 'Felhasználói fiók létrehozása';
$_['success_message']              = 'Gratulálunk! Sikeres bejelentkezés';

// Entry
$_['entry_email']                  = 'E-mail cím';
$_['entry_password']               = 'Jelszó';

// Error
$_['error_login']                  = 'Figyelmeztetés: A megadott felhasználónév vagy jelszó érvénytelen.';
$_['error_attempts']               = 'Figyelmeztetés: Sajnáljuk, de a bejelentkezési kísérletek száma elérte a maximumot. Kérjük próbálkozzon újra 1 óra múlva.';
$_['error_approved']               = 'Figyelmeztetés: A bejelentkezéshez jóváhagyásra van szükség.';