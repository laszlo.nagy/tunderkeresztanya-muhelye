<?php
// Szöveg
$_['text_title']                               = 'EC-Ship';
$_['text_weight']                              = 'Súly:';
$_['text_air_registered_mail']                 = 'Ajánlott légipostai küldés';
$_['text_air_parcel']                          = 'Légipostai küldés';
$_['text_e_express_service_to_us']             = 'e-Express szolgáltatás Amerikába';
$_['text_e_express_service_to_canada']         = 'e-Express szolgáltatás Kanadába';
$_['text_e_express_service_to_united_kingdom'] = 'e-Express szolgáltatás Angliába';
$_['text_e_express_service_to_russia']         = 'e-Express szolgáltatás Oroszországba';
$_['text_e_express_service_one']               = 'e-Express szolgáltatás';
$_['text_e_express_service_two']               = 'e-Express szolgáltatás';
$_['text_speed_post']                          = 'SpeedPost (Standard szolgáltatás)';
$_['text_smart_post']                          = 'Boríték küldés';
$_['text_local_courier_post']                  = 'Helyi postai küldés (gyűjtő szolgáltatás)';
$_['text_local_parcel']                        = 'Helyi csomag küldés';

// Hiba
$_['text_unavailable']                         = 'Nincs elérhető szállítási mód.';
