<?php
// Címsor
$_['heading_title']         = 'Sagepay Direkt kártya';

// Szöveg
$_['text_empty']		    = 'Önnek nincs mentve kártyája';
$_['text_account']          = 'Fiók';
$_['text_card']			    = 'SagePay Direkt kártya kezelés';
$_['text_fail_card']	    = 'Ahhoz, hogy törölni tudja a SagePay kártyáját, vegye fel a kapcsolatot az áruház üzemeltetőjével.';
$_['text_success_card']     = 'SagePay kártya sikeresen törölve';
$_['text_success_add_card'] = 'SagePay kártya sikeresen hozzáadva';

// Oszlop
$_['column_type']		    = 'Kártya típus';
$_['column_digits']	        = 'Kártya utolsó számai';
$_['column_expiry']		    = 'Kártya lejárati dátuma';

// Bejegyzés
$_['entry_cc_owner']        = 'Kártya kibocsátó';
$_['entry_cc_type']         = 'Kártya típus';
$_['entry_cc_number']       = 'Kártya szám';
$_['entry_cc_expire_date']  = 'Kártya lejárati dátuma';
$_['entry_cc_cvv2']         = 'Kártya biztonsági kódja (CVV2)';
$_['entry_cc_choice']       = 'Válasszon egy meglévő kártyát';

// Gomb
$_['button_add_card']       = 'Kártya hozzáadása';
$_['button_new_card']       = 'Új kártya hozzáadása';