<?php
// Címsor
$_['heading_title']				= 'Square Hitelkártya / Bankkártya';

// Szöveg
$_['text_account']				= 'Fiók';
$_['text_back']					= 'Vissza';
$_['text_delete']				= 'Törlés';
$_['text_no_cards']				= 'Nincs elmentett kártya adatok az adatbázisunkban.';
$_['text_card_ends_in']			= '%s kártya lejár ekkor &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; &bull;&bull;&bull;&bull; %s';
$_['text_warning_card']			= 'Megerősíti, hogy töröljük a kártyát? A következő fizetésnél újból hozzáadhatja.';
$_['text_success_card_delete']	= 'Sikeresen törölte a kártyát.';